# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  
  #########################################################
  # VM's configuration (i.e. Vrtualbox options)
  #########################################################
  
  config.vm.box = "ubuntu/bionic64"
  config.vm.box_check_update = false
  config.vm.provider "virtualbox" do |vb|
    vb.name = "BlackBox"
    vb.gui = false
    vb.memory = 1024
    vb.cpus = 2
  end
  
  #########################################################
  # Port Forwarding
  #########################################################
  
  # HTTP
  config.vm.network "forwarded_port", guest: 80, host: 8080
  # HTTPS
  config.vm.network "forwarded_port", guest: 443, host: 8443
  # InfluxDB
  config.vm.network "forwarded_port", guest: 8086, host: 8086
  # Grafana
  config.vm.network "forwarded_port", guest: 3000, host: 3000
  # Evebox
  config.vm.network "forwarded_port", guest: 5636, host: 5636
  
  #########################################################
  #Networking
  #########################################################
  
  # config.vm.network "private_network", ip: "192.168.33.10"
  config.vm.network "public_network", ip: "10.0.0.10", bridge: "br0"
  
  #########################################################
  # Provisioning (using Ansible)
  #########################################################
  
  # Ubuntu server config
  config.vm.provision "Ubuntu Server", type: 'ansible' do |ansible|
    ansible.playbook = "ubuntu-server.yml"
  end
  # InfluxDB
  config.vm.provision "InfluxDB", type: 'ansible' do |ansible|
    ansible.playbook = "influxdb.yml"
  end
  # Telegraf
  config.vm.provision "Telegraf", type: 'ansible' do |ansible|
    ansible.playbook = "telegraf.yml"
  end
  # Grafana
  config.vm.provision "Grafana", type: 'ansible' do |ansible|
    ansible.playbook = "grafana.yml"
  end
  config.vm.provision "Suricata", type: 'ansible' do |ansible|
    ansible.playbook = "suricata.yml"
  end
  config.vm.provision "Evebox", type: 'ansible' do |ansible|
    ansible.playbook = "evebox.yml"
  end
end
