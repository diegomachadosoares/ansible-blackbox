#!/usr/bin/env make

PLAYBOOK=playbook

all: clean check deploy

clean:
	find . -type f -name "*.retry" -exec rm -v {} ";"

check: ./$(PLAYBOOK).yml
	ansible-playbook ./$(PLAYBOOK).yml -i ./hosts --syntax-check ${ARGS}

dry: check
	ansible-playbook -i ./hosts ./$(PLAYBOOK).yml --check ${ARGS}

deploy: check
	ansible-playbook -i ./hosts ./$(PLAYBOOK).yml ${ARGS}

